﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.AppCenter;
using Microsoft.AppCenter.Analytics;
using Microsoft.AppCenter.Crashes;
using Xamarin.Forms;

namespace testapp2
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            MainPage = new Page1();
        }

        protected override void OnStart()
        {
            // Handle when your app starts
            AppCenter.Start("ios=ded81f21-ec67-4cd3-b2a7-8191a8e37421;android=df86348c-3a8e-4e67-92c4-c781d6e27977;uwp=707e47ed-5292-4ca4-b481-ea8fca785d46", typeof(Analytics), typeof(Crashes));

        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
